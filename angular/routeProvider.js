app.config(['$routeProvider', function($routeProvider) {

    $routeProvider.
      when('/home', {
        templateUrl: 'templates/home.html',
        controller: 'MainController'
    }).  
      when('/about', {
        templateUrl: 'templates/about.html',
        controller: 'MainController'
    }).  
      when('/agenda', {
        templateUrl: 'templates/agenda.html',
        controller: 'MainController'
    }).  
      when('/composers', {
        templateUrl: 'templates/composers.html',
        controller: 'MainController'
    }).  
      when('/music', {
        templateUrl: 'templates/music.html',
        controller: 'MainController'
    }).  
      when('/shop', {
        templateUrl: 'templates/shop.html',
        controller: 'MainController'
    }).  
      when('/checkout', {
        templateUrl: 'templates/checkout.html',
        controller: 'MainController'
    }).        
    when('/basket', {
        templateUrl: 'templates/basket.html',
        controller: 'MainController'
    }).      
      when('/dtail/:serial', {
        templateUrl: 'templates/dtail.html',
        controller: 'MainController'
    }). 
      when('/contact', {
        templateUrl: 'templates/contact.html',
        controller: 'MainController'
    }).  
      otherwise({
        redirectTo: '/home'
    });
}]); 
