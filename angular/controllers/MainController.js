app.controller('MainController', ['$scope', '$route', '$http', '$rootScope', function ($scope, $route, $http, $rootScope) {
  
    $scope.$on('$routeChangeSuccess', function() { 
        //get routing and save in scope
        $scope.route = $route.current;
        
        $scope.currentPageString = $scope.route.loadedTemplateUrl;
        $scope.currentPageArray = $scope.currentPageString.split(/[/.]+/);
        $scope.currentpage = $scope.currentPageArray[1].toString();
        //this is the current page in a object
        console.log($scope.currentpage);
        data = {"q": $scope.currentpage};

        //request current pageinfo from database
        $http.post('database/request.db.php',  data).success(function(data, status, headers, config){
//            console.log(status + ' - ' + data);
//                console.log("page " + data);
                $scope.content = data;
        }).error(function(data, status, headers, config){
            console.log('error');
        });
    });
    
        $http.post('database/footer.db.php').success(function(data, status, headers, config){
//            console.log(status + ' - ' + data);
//                console.log("footer " + data);
                $scope.footer = data;
        }).error(function(data, status, headers, config){
            console.log('error');
        });
    
    
    //gets serial from url for detail page view
//    $scope.urlserial = getserial();

    //reloads view to keep content up to date    
    $scope.reloadRoute = function () {
        $route.reload();
    //    console.log('reloadRoute');
    };    

    // when 1 scope has been readed and some function  wont be done
    $scope.readed = 0;

    $scope.$on('$viewContentLoaded', function(){
        $http.get('php/json/productjson.php')
        .then(function(res){
            $scope.productjson = res.data;   
            console.log('json loaded'); 
            $scope.product = $scope.productjson;
            $scope.readed++;
            $scope.refreshDataBasket();
        });
    });
    
    
    //add and delete items from cookies #cookies
    $scope.addBasketItem = function(serial){
        basket.addItem(serial);
        $scope.refreshDataBasket();
        window.location.href = '#basket';
        $scope.reloadRoute();
    }

    $scope.removeBasketItem = function(serial){
        basket.removeItem(serial);
        $scope.refreshDataBasket();
        $scope.reloadRoute();
    }

    $scope.emptyBasket = function(){
         basket.empty();
        $scope.refreshDataBasket();
        $scope.reloadRoute();
    }
    //#cookies end

    //read basket and filter input for good output
    $scope.refreshDataBasket = function(){

        //console.log('ik refresh.controller');

        //this wil get the string from the cookie and make a array
        var basketitems = basket.read();

        if(basketitems === null)
        {
            basketitems = [];   
        } 
        else if(basketitems === "")
        {
            basketitems = []; 
        } 
        else if(basketitems.charAt(0) === "|")
        {
            basketitems = basketitems.substring(1).split("|");
        }
        else 
        {
            basketitems = basketitems.split("|");
        }

        //items in basket
        $scope.basketitems = basketitems;

        //nav basket counter
        $scope.basketnavcount = basketitems;

        //sorts basket array 0 to >
        basketitems.sort();

        //counts how many of the same product there are and puts these into a array
        var current = null;
        var cnt = 0;
        var arraycnt = 0;
        var amountPerProduct = [];
        for (var i = 0; i <$scope.basketitems.length; i++) {
            if ($scope.basketitems[i] != current) {
                if (cnt > 0) 
                {
                    amountPerProduct[arraycnt] = cnt;
                    arraycnt++;
                }
                    current =$scope.basketitems[i];
                    cnt = 1;
                } 
                else 
                {
                    cnt++;
                }
        }
        if (cnt > 0) 
        {
            amountPerProduct[arraycnt] = cnt;
            arraycnt++;
        }

        //al serials with the same value are trown uit of the array
        $scope.basketitems = $scope.basketitems.filter( function( item, index, inputArray ) {
               return inputArray.indexOf(item) == index;
        });

        //array for product amount per serial
        $scope.amountPerProduct = amountPerProduct;

        //make product string for order details database
        var pcount = 0;
        var parray = [];
        for (var i = 0; i <$scope.basketitems.length; i++){
            parray[pcount] = $scope.basketitems[pcount] + "-" + $scope.amountPerProduct[pcount];  
            pcount++;
        }

        //database product string
        $rootScope.products = parray.join('|');
        cookies.create('basketproducts',$scope.basketitems,3);
        cookies.create('basketproductcount',$scope.amountPerProduct,3);
    }

    //makes array for amount per product in basket
    $scope.basketNR = -1;
    $scope.basketnr = function(){
        if(!$scope.readed == 0){
            count = $scope.basketNR;
            count++
            $scope.basketNR++;
            return count;
        }
    }


    //calculates price for basket and checkout/database
    $scope.subtotal = 0;
    $scope.shippingtotal = 0;
    $scope.total = 0;
    $scope.amountcount = 0;

    $scope.calculateBasketPrice = function(price , shippingcost){
        if($scope.readed == 1){
    //        console.log('ik tel')
           $scope.subtotal = $scope.subtotal + (price * $scope.amountPerProduct[$scope.amountcount]);
           $scope.shippingtotal = $scope.shippingtotal + (shippingcost * $scope.amountPerProduct[$scope.amountcount]);
           $scope.total = $scope.subtotal + $scope.shippingtotal;
           $scope.amountcount++;
        }
    }

    //checkoutpage---------------------------------------//

    //check invalid fields when submiting checkout form
    $scope.mp = function(user) {
        angular.forEach($scope.userForm.$error.required, function(field) {
            field.$setDirty();
        });
    }

    //when form is correct save in master
    $scope.update = function(user) {
        $scope.master = angular.copy(user);
    };

    //delete content of master **not in use** 
    //$scope.reset = function() {
    //    $scope.user = angular.copy($scope.master);
    //};


    //sends data to database when form correct else gives error
    $scope.submitForm = function() {
        if ($scope.userForm.$valid) {
                //puts product, shippingcost, subtotal and total in master
                $scope.master.products      = $rootScope.products;
                $scope.master.shippingcost  = $scope.shippingtotal;
                $scope.master.subtotal      = $scope.subtotal;
                $scope.master.total         = $scope.total;

                //puts master in data
                data = $scope.master;

            //sends data to payment cyclus
            $http.post('http://clients.newartsint.com/dutch-piccolo/php/payment/index.php',  data)
            .success(function(data, status, headers, config)
            {           
                console.log(data);
                console.log("url =" + data[0]);
                window.location.href = data[0];
            })
            .error(function(data, status, headers, config)
            {
                console.log('post failed ' + data);
            });
        }   
        else 
        {
            console.log("form incorrect");
        }
    }
   
}]);
    
