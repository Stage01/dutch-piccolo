//cookie functions
var cookies = {
    
     'create' : function(name,value,days){
        if(days){
            var date = new Date();
            
            date.setTime(date.getTime()+(days*24*60*60*1000));
            
            var expires = "; expires="+date.toGMTString();
            
        }   else 
            
            var expires = "";
         
            document.cookie = name+"="+value+expires+"; path=/";
     },

    'read': function(name){
        
        var nameEQ = name + "=";

        var ca = document.cookie.split(';');

            for(var i=0;i < ca.length;i++) {

                var c = ca[i];

                while (c.charAt(0)==' ')
                    c = c.substring(1,c.length);
                
                    
                if (c.indexOf(nameEQ) == 0)
                    return c.substring(nameEQ.length,c.length);

            }
            return null;
    },

    'erase': function(name) {
        this.create(name,"",-1);
    }

}

//basket functions 
var basket = {
    
    'create': function(value){
        cookies.create('basket',value,3)
    },
    
    'read': function(){
        return cookies.read('basket')
    },
    
    'addItem': function(newitem){
        if(this.read() == null) {
            this.create(newitem)
        }   else {
            this.create(this.read() + "|" + newitem)
        }
    },

    'removeItem': function (remove){
        var split = this.read().split("|")
        for (var c in split){
           if(split[c] == remove){
               split.splice(c, 1)
               this.create(split.join('|'))
               break;
           }
        }
    },

    'empty': function(){
        cookies.erase('basket');
    }
    
}