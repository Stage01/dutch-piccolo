$(document).ready(function() { 
    /* loads lightbox properties */
    $(".fancybox").fancybox();
    
    /* switch between desktop and mobile menu */
    hideDiv();
    hideicon();
    
    $('.mobilenavbar').click(function() {
                    $('.menuoverlay').show();
    });
            
    $('.mobilenavbarOverlay').click(function() {
            $('.menuoverlay').hide();
    });
    
    $('.overlaymenulist').click(function() {
            $('.menuoverlay').hide();
    });
});

$(window).resize(function(){
            hideDiv();
            hideicon();
});

/* switch between desktop and mobile menu */
function hideDiv(){
    if ($(window).width() < 500) {
            $(".navbar").hide();
    }else{
        $(".navbar").show();
    }
}
/* switch between desktop and mobile menu */
function hideicon(){
    if ($(window).width() < 500) {
            $(".mobilenavbar").show();
    }else{
        $(".mobilenavbar").hide();
    }
}

getserial = function(){
    var url = window.location.href;
    var urlarray = url.split('/');
    var urllenght = urlarray.length;
    serial = urlarray[5];
    return parseInt(serial);
}


        