<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);


class Database{
    
    public function __construct(){        //initialize   
        global $_settings;
        $this->settings = $_settings;
        $this->mysqli = $this->dbConnect();
    }
    
    public function dbConnect(){        
        
        $settings = $this->settings;
        
        $conn = new mysqli( $settings['db_host'], $settings['db_user'], $settings['db_password'], $settings['db_database']);
           
        if ($conn->connect_error) 
        {
            die($conn->connect_errno ." ". $conn->connect_error);
        } 
            else 
        {
            return $conn;
        }   
    }
    
    public function dbClose(){
        
        $this->mysqli->close();   
        
    }
    
    public function getQuery($q){
        
        if ($data = $this->mysqli->query($q)) {
            $count = 0;
            $array = array();
            /* fetch associative array */
            while ($row = $data->fetch_assoc()) {
                foreach($row as $key=>$val){
                   $result[$count][$key] = $val;
                }
                $count++;
            }
            return $result;
        }
        
    }
    
    public function makeQuery($q){
        if (mysqli_query($this->mysqli, $q)) {
//            echo "New record created successfully";
        } else {
            echo "Error: " . $q . "<br>" . mysqli_error($this->mysqli);
        }   
    }
    
    public function encrypt($decrypted, $password, $salt) {
        // Build a 256-bit $key which is a SHA256 hash of $salt and $password.
        $key = hash('SHA256', $salt . $password, true);
        // Build $iv and $iv_base64.  We use a block size of 128 bits (AES compliant) and CBC mode.  (Note: ECB mode is inadequate as IV is not used.)
        srand(); $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC), MCRYPT_RAND);
        if (strlen($iv_base64 = rtrim(base64_encode($iv), '=')) != 22) return false;
        // Encrypt $decrypted and an MD5 of $decrypted using $key.  MD5 is fine to use here because it's just to verify successful decryption.
        $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $decrypted . md5($decrypted), MCRYPT_MODE_CBC, $iv));
        // We're done!
        return $iv_base64 . $encrypted;
    }

    public function decrypt($encrypted, $password, $salt) {
        //echo $encrypted."<br>";
        if($encrypted!=''){
            // Build a 256-bit $key which is a SHA256 hash of $salt and $password.
            $key = hash('SHA256', $salt . $password, true);
            // Retrieve $iv which is the first 22 characters plus ==, base64_decoded.
            $iv = base64_decode(substr($encrypted, 0, 22) . '==');
            // Remove $iv from $encrypted.
            $encrypted = substr($encrypted, 22);
            // Decrypt the data.  rtrim won't corrupt the data because the last 32 characters are the md5 hash; thus any \0 character has to be padding.
            $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($encrypted), MCRYPT_MODE_CBC, $iv), "\0\4");
            // Retrieve $hash which is the last 32 characters of $decrypted.
            $hash = substr($decrypted, -32);
            // Remove the last 32 characters from $decrypted.
            $decrypted = substr($decrypted, 0, -32);
            // Integrity check.  If this fails, either the data is corrupted, or the password/salt was incorrect.
            if (md5($decrypted) != $hash) return false;
            // Yay
        } else {
            $decrypted = '';
        }
        return $decrypted;
    }
}

?>