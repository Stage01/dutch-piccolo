<?php
    // show errors
    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    //database functions object
    include_once("config.php");
    include_once("database.class.php");

    global $_settings;
    $settings = $_settings;
    
    $author = $settings['author'];
    //database object
    $db = new Database;

    //select what you want
    $data = json_decode(file_get_contents("php://input"));
    
   
    if(!isset($data->q)){
     $page = "agenda";   
    }else{
     $page = $data->q;   
    }

    $query = array();
    switch ($page) {
        case "home":
            $request = "SELECT * FROM generic WHERE author='".$author."' AND hook ='about' AND check_active='1' AND serialize_serialized_nieuwskenmerk LIKE '%1433765621%' ORDER BY date_news DESC
";
            $query_temp = $db->getQuery($request);
            $query[0]['introduction_lan1'] = urldecode($query_temp[0]['introduction_lan1']);
            
            $request = "SELECT * FROM video WHERE author='".$author."' AND title_lan1 = 'Dutch Piccolo Project' AND check_active='1' AND serialize_serialized_nieuwskenmerk LIKE '%1433765621%' ORDER BY date_news DESC";
            $query_temp = $db->getQuery($request);
            $query[0]['video_youtube'] = $query_temp[0]['video_youtube'];
            
            $request = "SELECT * FROM nieuws WHERE author='".$author."'" ;
            $query_temp = $db->getQuery($request);
            $query[0]['news'][0] = $query_temp[0]['introduction_lan1'];
            $query[0]['news'][1] = $query_temp[1]['introduction_lan1'];
            
            $request = "SELECT * FROM agenda_data WHERE author='".$author."'" ;
            $query_temp = $db->getQuery($request);
            $count = 0;
            foreach($query_temp as $value){
                if($count < 2){
                    $query[0]['agenda'][$count] = $value;
                    $count++;
                }
            }

            break;
        case "about":
            $request = "SELECT * FROM generic WHERE author='".$author."' AND hook = 'about' AND check_active='1' AND serialize_serialized_nieuwskenmerk LIKE '%1433765621%' ORDER BY date_news DESC";
            $query_temp = $db->getQuery($request);
            $query[0]['description_lan1'] = urldecode($query_temp[0]['description_lan1']);
            
            $request = "SELECT * FROM video WHERE author='".$author."' AND title_lan1 = 'Dutch Piccolo Project' AND check_active='1' AND serialize_serialized_nieuwskenmerk LIKE '%1433765621%' ORDER BY date_news DESC";
            $query_temp = $db->getQuery($request);
            $query[0]['video_youtube'] = $query_temp[0]['video_youtube'];
            break;
        case "agenda":
            $request = "SELECT * FROM agenda_data WHERE author='".$author."'ORDER BY serial" ;
            $query_temp = $db->getQuery($request);
            $count = 0;
            foreach($query_temp as $value){
                if($count < 10){
                    $query[0]['agenda'][$count] = $value;
                    $count++;
                }
            }
            
            $request = "SELECT * FROM video WHERE author='".$author."' AND title_lan1 = 'Dutch Piccolo Project'";
            $query_temp = $db->getQuery($request);
            $query[0]['video_youtube'] = $query_temp[0]['video_youtube'];
            
            $request = "SELECT * FROM nieuws WHERE author='".$author."'" ;
            $query_temp = $db->getQuery($request);
            $query[0]['news'][0] = $query_temp[0]['introduction_lan1'];
            $query[0]['news'][1] = $query_temp[1]['introduction_lan1'];
            break;
        case "composers":
            $request = "SELECT * FROM new_artists WHERE author='".$author."'";
            $query_temp = $db->getQuery($request);
            $count = 0;
            foreach($query_temp as $value){
                if($count < 40){
                    $value['description'] = urldecode($value['description_lan1']);
                    $query[0]['composer'][$count] = $value;
                    $count++;
                }
            }

            $request = "SELECT * FROM video WHERE author='".$author."' AND title_lan1 = 'Dutch Piccolo Project'";
            $query_temp = $db->getQuery($request);
            $query[0]['video_youtube'] = $query_temp[0]['video_youtube'];
            
            $request = "SELECT * FROM nieuws WHERE author='".$author."'" ;
            $query_temp = $db->getQuery($request);
            $query[0]['news'][0] = $query_temp[0]['introduction_lan1'];
            $query[0]['news'][1] = $query_temp[1]['introduction_lan1'];
            break;
        case "music":
            $request = "SELECT * FROM video WHERE author='".$author."' AND title_lan1 = 'Dutch Piccolo Project'";
            $query_temp = $db->getQuery($request);
            $query[0]['video_youtube'] = $query_temp[0]['video_youtube'];
            
            $request = "SELECT * FROM nieuws WHERE author='".$author."'" ;
            $query_temp = $db->getQuery($request);
            $query[0]['news'][0] = $query_temp[0]['introduction_lan1'];
            $query[0]['news'][1] = $query_temp[1]['introduction_lan1'];
            break;
        case "shop":
           $request = "SELECT * FROM video WHERE author='".$author."' AND title_lan1 = 'Dutch Piccolo Project'";
            $query_temp = $db->getQuery($request);
            $query[0]['video_youtube'] = $query_temp[0]['video_youtube'];
            
            $request = "SELECT * FROM nieuws WHERE author='".$author."'" ;
            $query_temp = $db->getQuery($request);
            $query[0]['news'][0] = $query_temp[0]['introduction_lan1'];
            $query[0]['news'][1] = $query_temp[1]['introduction_lan1'];
            break;
        case "contact":
            $request = "SELECT * FROM generic WHERE author='".$author."' AND hook = 'contact'AND check_active='1' AND serialize_serialized_nieuwskenmerk LIKE '%1433765621%' ORDER BY date_news DESC";
            $query_temp = $db->getQuery($request);
            $query[0]['contact'] = urldecode($query_temp[0]['description_lan1']);
            
            $request = "SELECT * FROM video WHERE author='".$author."' AND title_lan1 = 'Dutch Piccolo Project'";
            $query_temp = $db->getQuery($request);
            $query[0]['video_youtube'] = $query_temp[0]['video_youtube'];
            
            $request = "SELECT * FROM nieuws WHERE author='".$author."'" ;
            $query_temp = $db->getQuery($request);
            $query[0]['news'][0] = $query_temp[0]['introduction_lan1'];
            $query[0]['news'][1] = $query_temp[1]['introduction_lan1'];
            break;
        default:
           echo "no match found";
    }
    
   echo json_encode($query);
    
    $db->dbClose();
?>