<?php
    // show errors
    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    //database functions object
    include_once("../config.php");
    include_once("../classes/database.class.php");

    global $_settings;
    $settings = $_settings;
//    echo "test";

    //database object
    $db = new Database;
    
    $query = "SELECT * From new_orders_external WHERE serial='1445608823'";
    $result = $db->getQuery($query);
    $data = $result[0];
    $linkduration = urlencode($db->encrypt(strtotime("7days"), $settings['crypt_password'], $settings['salt']));
    $randomid = urlencode($db->encrypt(mt_rand(), $settings['crypt_password'], $settings['salt']));
    
    if($data['downloads'] == !""){
        $download = "<h2 style='text-align:center; padding-top:20px;' ><a style='padding:20px; color:#555555; text-decoration:none; ' href='http://clients.newartsint.com/vflow/webshop/payment/get.php?code=" .urlencode($data['downloads'])."&timecode=".$linkduration."&uid=".$randomid."'>Click here for your downloads</a></h2>";
    }
    else
    {
        $download = "";   
    }

    $email ="<div style='font-family:helvetica, arial; padding:20px; color:#555555; width:600px; font-size:13px; line-height:1.5;'>
      
                <div width='100%' style='padding:20px;'><img src='http://clients.newartsint.com/vflow/webshop/img/logo.png' width='600px;'></div>
                <br><hr style='width:640px;'><br>

                <h1 style='color:#a2a2a2;'>Thank you for your order</h1>
                <p>Dear customer,<br><br>
                Thank you for your order at <a style='color:blue; text-decoration:none;'href='http://www.google.nl'>vflow.nl</a></p>
                <div style='background:#f6f6f6; height:auto; width:100%; padding:20px;'>
                <p style='color:#a2a2a2; font-size:18px;'>Address</p>
                <p>".$data['name_first']." ". $data['name_last']."<br>".
                $data['street']." ". $data['street_nr']."<br>".
                $data['zipcode']."<br>".
                $data['city'].", ". $data['country']."</p>
    
                <p>Order date: ". date('d-m-Y H:i:s', $data['serial'])."<br>
                Order number: ". $data['order_nr']."</p><hr>".
                urldecode($data['description_lan1']).

                "<hr><p style='text-align:right;'>Subtotal: &euro;". money_format('%.2n', $data['price_subtotal'])."<br>
                Shipping: &euro;". money_format('%.2n', $data['price_shipping'])."<br><br>
                total: &euro;". money_format('%.2n', $data['price_total']). "</p>
            </div>
            ".$download."
            
   </div>";

//    echo $email;
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
//    mail('stage.web@newartsint.com', 'is gelukt', $email , $headers);

?>