<?php
   

    //database functions object
    include_once("../config.php");
    include_once("../classes/database.class.php");

    global $_settings;
    $settings = $_settings;

	// show errors
    error_reporting(E_ALL);
    ini_set('display_errors', 1);


    //database object
    $db = new Database;
    $query = "SELECT * FROM new_products_external 
    WHERE 

        author='ilonkakolthof' AND 
        check_active = 1
    
    ORDER BY date_created DESC";
    
    $result = $db->getQuery($query);
    
    $c = 0;
    foreach($result as $value){
        if($value['image_01'] == null){
            $result[$c]['image_01'] = $settings['noproduct'];
        }
        else
        {
            $result[$c]['image_01'] = "http://cms.new-art.nl/content/img/new_products_external/".$value['image_01'].".jpg"; 
        }
        
        if($value['image_02'] == null){
            $result[$c]['image_02'] = "";
        }
        else
        {
            $result[$c]['image_02'] = "http://cms.new-art.nl/content/img/new_products_external/".$value['image_02'].".jpg"; 
        }
        
        if($value['image_03'] == null){
            $result[$c]['image_03'] = "";
        }
        else
        {
            $result[$c]['image_03'] = "http://cms.new-art.nl/content/img/new_products_external/".$value['image_03'].".jpg"; 
        }
        
        if($value['image_04'] == null){
            $result[$c]['image_04'] = "";
        }
        else
        {
            $result[$c]['image_04'] = "http://cms.new-art.nl/content/img/new_products_external/".$value['image_04'].".jpg"; 
        }
        
        if($value['image_05'] == null){
            $result[$c]['image_05'] = "";
        }
        else
        {
            $result[$c]['image_05'] = "http://cms.new-art.nl/content/img/new_products_external/".$value['image_05'].".jpg"; 
        }
        
         $result[$c]['description_lan1'] = urldecode($value['description_lan1']);
        if($value['check_download'] == 1)
        {
            $result[$c]['check_download'] = '(download)';
        } 
        else
        {
            $result[$c]['check_download'] = '';   
        }
        

        $c++;
    }

    header('Content-Type: application/json');
    echo json_encode($result);

?>