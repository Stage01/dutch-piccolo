
<?php  
    //database functions object
    require("../config.php");
    require("../classes/database.class.php");
    require("../classes/smtpmailer.class.php");
    
    //get settings
    global $_settings;
    $settings = $_settings;

    //database object
    $db = new Database;

    //get encrypet data from url
    $serials =  @htmlspecialchars($_GET["code"]);
    $timecode = @htmlspecialchars($_GET["timecode"]);
    $uid = @htmlspecialchars($_GET["uid"]);

    //decrypt data from url
    $serials = $db->decrypt($serials, $settings['crypt_password'], $settings['salt']);
    $timecode = $db->decrypt($timecode, $settings['crypt_password'], $settings['salt']);
    $uid = $db->decrypt($uid, $settings['crypt_password'], $settings['salt']);

    //put serials in array
    $serials = explode(',',$serials);
    
    //check if timecode does not exeed 7 days
    if(!time() < $timecode){
        //echo "valid time code <br>";  
        //check if zip has already been made
        if(file_exists("../../downloadsessions/".$uid.".zip"))
        {
            //echo "file already exist give ";          
            $message = '<div class="download" style="text-align:center; font-family:arial;">
                            <p>thank you for you download click on the download link below to download your order</p>
                            <a style="color:black; text-decoration:none;" href="http://www.v-flow.nu/shop/php/dowload/download.php?uid='.$uid.'"><button>Click here to download</button></a> 
                        </div>';
        } 
        else 
        {
            //makes new zip 
            $zip = new ZipArchive;
            
            //checks if the new zip could be made
            if (!$zip->open('../../downloadsessions/'.$uid.'.zip',  ZipArchive::CREATE)) 
            { 
            //echo 'could not create zip!';
            } 
            else 
            {    
                $i = 0;
                $productError = false;
                //loops serials gets the product filepath
                foreach($serials as $value){
                    $query ="SELECT file_01 FROM new_products_external where serial='".$value."'";
                    $result = $db->getQuery($query);
                    $filename = $result[0]['file_01'];
                    $currentPath = 'http://cms.new-art.nl/content/files/vloeimans/'.rawurlencode($filename);
                    //checks if the current filepath is readable
                    if(!$download_file = @file_get_contents($currentPath))
                    {
//                        echo "<br>the file does not exist send mail to admin"; 
                        $productError = true;
                        //send message to admin about problem
                        $email ="
                            the file".$currentPath." was requested but does not exist or the path is not correct.
                        ";
                        
                        $to = 'thomas.heijmans@newartsint.com';
                        $from = "auto-error-massage@v-flow.nu";
                        $subject = "Your order at V-Flow.nu";
                        $body = $email;
                        $SMTPMail = new smtpMailer($to,$from,$subject,$body);
                        break;
                    }
                    else
                    {   
                        //checks if new file has been added to zip
                        if(!$zip->addFromString($filename,$download_file)){
//                            echo "could not add $currentPath ";
                        } else {
//                            echo $value.'.pdf added <br>';
                        }
                        $i++;   
                    }
                }
                
                //checks if there has been a error in the process
                if($productError == true){
                    $message = '<div class="download" style="text-align:center; font-family:arial;">
                                    <p>Something went wrong with setting up your order. An mail has been send to the administrator.</p>
                                    <p>If you do not get a respons within 3 days please contact us at problem@V-Flow.nl</p>
                                </div>';   
                    //tell there is an error and that they will be contacted
                }
                else
                {
                    $zip->close();
//                  echo '<br>Archive created!';   
                    $message = '<div class="download" style="text-align:center; font-family:arial;">
                                    <p>thank you for you download click on the download link below to download your order</p>
                                    <a style="color:black; text-decoration:none;" href="http://www.v-flow.nu/shop/php/download/download.php?uid='.$uid.'"><button>Click here to download</button></a> 
                                </div>';
                }
            }
        }
    }
    else
    {
//        echo "verlopen";
    } 

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>webshop</title>
    <link rel="stylesheet" href="../../css/download.css"> 
  </head>
  <body ng-app="webshop">
 
    <div class="wrapper" ng-controller="MainController" id="MainController">
        <div class="row header">Downloads</div>
            <?php echo $message ?>
        <div class="push"></div>
    </div>
      
    <footer>
        <div class="container">
            <p class="col-xs-12"></p>
        </div>
    </footer>
    
  </body>
</html>
