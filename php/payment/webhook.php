<?php
    
//    error_reporting(E_ALL);
//    ini_set('display_errors', 1);

    //database functions object
    require("../config.php");
    require("../classes/database.class.php");
    require("../classes/smtpmailer.class.php");
    //mollie object
    if(!require("../Mollie/API/Autoloader.php")){
        echo" mollie api not loaded";   
    };

    global $_settings;
    $settings = $_settings;

    //new database object
    $db = new Database;

    //make mollie object an insert api
    $mollie = new Mollie_API_Client;
    $mollie->setApiKey($settings['apikey']);
  
//try to find order
try
{
    //get meta-data from order_id
	$payment  = $mollie->payments->get($_POST["id"]);
	$order_id = $payment->metadata->order_id;
    $adres = $payment->metadata->email;
    $serial = $payment->metadata->serial;
    
    //get order from database
    $query = "SELECT * From new_orders_external WHERE serial='".$serial."'";
    $result = $db->getQuery($query);
    $data = $result[0];
    //creates data for get page
    $linkduration = urlencode($db->encrypt(strtotime("7days"), $settings['crypt_password'], $settings['salt']));
    $randomid = urlencode($db->encrypt(mt_rand(), $settings['crypt_password'], $settings['salt']));
    
    //if the order does not contain a download do not create donwload link
    if($data['downloads'] == "no downloads"){
        $download = ""; 
    }
    else
    {
//       $download = "<h2 style='text-align:center; padding-top:20px;' ><a style='padding:20px; color:#555555; text-decoration:none; ' href='http://v-flow.nu/shop/php/download/get.php?code=" .urlencode($data['downloads'])."&timecode=".$linkduration."&uid=".$randomid."'>Click here for your downloads</a></h2>";   
        $download = ""; 
    }

    //make mail for when payment is paid
    $email ="<div style='font-family:helvetica, arial; padding:20px; color:#555555; width:600px; font-size:13px; line-height:1.5;'>
                <h1 style='color:#a2a2a2;'>Thank you for your order</h1>
                <p>Dear customer,<br><br>
                Thank you for your order at <a style='color:blue; text-decoration:none;'href='########'>dutch piccolo</a></p>
                <div style='background:#f6f6f6; height:auto; width:100%; padding:20px;'>
                <p style='color:#a2a2a2; font-size:18px;'>Address</p>
                <p>".$data['name_first']." ". $data['name_last']."<br>".
                $data['street']." ". $data['street_nr']."<br>".
                $data['zipcode']."<br>".
                $data['city'].", ". $data['country']."</p>
    
                <p>Order date: ". date('d-m-Y H:i:s', $data['serial'])."<br>
                Order number: ". $data['order_nr']."</p><hr>".
                urldecode($data['description_lan1']).

                "<hr><p style='text-align:right;'>Subtotal: &euro;". money_format('%.2n', $data['price_subtotal'])."<br>
                Shipping: &euro;". money_format('%.2n', $data['price_shipping'])."<br><br>
                total: &euro;". money_format('%.2n', $data['price_total']). "</p>
            </div>
            ".$download."
            
   </div>";

    // when payment is succes
	if ($payment->isPaid() == TRUE)
	{
        $to = $adres;
        $from = "no-reply@dutchpiccolo.nl";
        $subject = "Your order at dutchpiccolo.nl";
        $body = $email;
        $SMTPMail = new smtpMailer($to,$from,$subject,$body);
//        $SMTPMail = new smtpMailer('thomas.heijmans@newartsint.com',$from,$subject,$body);
        $query = "UPDATE new_orders_external SET order_status='betaalt' WHERE order_nr='".$order_id."'";
        $db->makeQuery($query);
	}
    //when payment is closed
	elseif ($payment->isOpen() == FALSE)
	{
        $query = "UPDATE new_orders_external SET order_status='afgebroken' WHERE order_nr='".$order_id."'";
        $db->makeQuery($query);
	}
}
//when order is not found
catch (Mollie_API_Exception $e)
{
	echo "API call failed: " . htmlspecialchars($e->getMessage());
}

?>