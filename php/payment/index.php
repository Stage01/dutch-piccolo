<?php 
    error_reporting(E_ALL);
    ini_set("display_errors", 1);

    //database functions object
    include_once("../config.php");
    include_once("../classes/database.class.php");
    include_once("../Mollie/API/Autoloader.php");
//    require("../classes/smtpmailer.class.php");


    //database object
    $db = new Database;

    global $_settings;
    $settings = $_settings;

    //gets al information needed to make descriptionlan 1
    $products =  $_COOKIE['basketproducts'];
    $cPerProduct = $_COOKIE['basketproductcount'];

    //get products
    $query = "SELECT * FROM new_products_external 
            WHERE author='".$settings['author']."' 
            AND check_active = 1
            ORDER BY date_created DESC ";

    $json = $db->getQuery($query);
    
    //parse product values
    $c = 0;
    foreach($json as $value){
        
        $json[$c]['description_lan1'] = urldecode($value['description_lan1']);
        
        if($value['image_01'] == null){
            $json[$c]['image_01'] = $settings['noproduct'];
        }
        else{
            $json[$c]['image_01'] = "http://cms.new-art.nl/content/img/new_products_external/".$value['image_01'].".jpg"; 
        }
        
        if($value['image_02'] == null){
            $json[$c]['image_02'] = "";
        }else{
            $json[$c]['image_02'] = "http://cms.new-art.nl/content/img/new_products_external/".$value['image_02'].".jpg"; 
        }
        
        if($value['image_03'] == null){
            $json[$c]['image_03'] = "";
        }else{
            $json[$c]['image_03'] = "http://cms.new-art.nl/content/img/new_products_external/".$value['image_03'].".jpg"; 
        }
        
        if($value['image_04'] == null){
            $json[$c]['image_04'] = "";
        }else{
            $json[$c]['image_04'] = "http://cms.new-art.nl/content/img/new_products_external/".$value['image_04'].".jpg"; 
        }
        
        if($value['image_05'] == null){
            $json[$c]['image_05'] = "";
        }else{
            $json[$c]['image_05'] = "http://cms.new-art.nl/content/img/new_products_external/".$value['image_05'].".jpg"; 
        }
        
        if($value['check_download'] == 1){
            $json[$c]['check_download'] = 'download';
        }else{
            $json[$c]['check_download'] = '';   
        }
         
        $c++;
    }


    //get checkoutform input
    $data = json_decode(file_get_contents("php://input"), TRUE);
    //url encode comments
    if(isset($data['comments'])){
        $description = urlencode($data['comments']);
    }else{
        $description = '';   
    }
    //makes a uniek serial
    $serial = time();
    
    //when there are not comments say so
    if(!isset($data['descption_notes'])){
        $data['desciption_notes'] = "no comments";
    }

    //when shipping is niet selected say so
    if(!isset($data['firstname_s'])){
          $data['firstname_s'] = 'not given';
          $data['lastname_s'] = 'not given';
          $data['street_s'] = 'not given';
          $data['street_nr_s'] = 'not given';
          $data['zipcode_s'] = 'not given';
          $data['city_s'] = 'not given';
          $data['country_s'] = 'not given';
          $data['shipping'] = 0;
    }
    
    //puts info about products in array
    $products = explode(",",$products);
    $cPerProduct = explode(",",$cPerProduct);

    //counter for product in origenal array
    $i = 0;

    //counter for products in new array
    $c = 0;

    //counter for download serial array
    $d = 0;

    //json with products in basket
    $newjson = array();

    //array with serials that are a download
    $downloadSerials = array();

    //loops products
    foreach($json as $value){
        $ii = 0;
        //loop serials
        foreach($products as $value){
            // if serial basket = serial products put in new array
            if( $json[$i]['serial'] ==  $products[$ii]){
                // if type of match is download put in download serial array
                if($json[$i]['check_download'] == "download"){
                   $downloadSerials[$d] = $json[$i]['serial'];
                   $d++;
                }
                $newjson[$c] = $json[$i];
                $ii++;
                $c++;
            }
            else
            {
                $ii++;
            }
        }
      $i++;
    }

    //makes html table for in the mail
    $c = 0;

    $mailcontent = "<table border=0 id='baskettable' width=100% style='font-size:12px; color:#555555'>";

    //loops content from new array to make rows for table
    foreach($newjson as $value){
        $mailcontent .= "
                <tr style=''>
                    <td width='20%' height='70'><img src='".$newjson[$c]['image_01']."' width='100px'></td>
                    <td width='50%'>".$newjson[$c]['title_lan1']."</td>
                    <td width='10%'>".$cPerProduct[$c]." x</td>
                    <td width='10%'>&euro;".money_format('%.2n', $newjson[$c]['price'])."</td>
                    <td width='10%'>&euro;".money_format('%.2n', $newjson[$c]['price'] * $cPerProduct[$c])."</td>
                </tr>
            ";
        $c++;
    };

    $mailcontent .= "</table>";
    //encodes html table for inserting in database

    $mailEncode = urlencode($mailcontent);
    //incrypt download serials

    if( count($downloadSerials) > 0){
        $downloadSerials = $db->encrypt(implode(",",$downloadSerials), $settings['crypt_password'], $settings['salt']);
    }else{
        $downloadSerials = "no downloads";   
    }

    //query for inserting data in database
    $query = "INSERT INTO new_orders_external (serial, title_lan1, date_created, author, description_lan1, description_notes, products, downloads, price_shipping, price_subtotal, price_total, order_status, shipping, name_first, name_last, street, street_nr, zipcode, city, country, email, name_first_shipping, name_last_shipping, street_shipping, street_nr_shipping, zipcode_shipping, city_shipping, country_shipping, ip)
                                       VALUES ('". $serial ."', '".date_format(date_create(), 'Y-m-d'). " " .$data['firstname']. " ". $data['lastname'] ."', '". time() ."', '".$settings['author']."', '".$mailEncode."', '".$description."', '".$data['products']."', '". $downloadSerials."', '".$data['shippingcost']."', '".$data['subtotal']."', '".$data['total']."', 'Processed', '".$data['shipping']."', '".$data['firstname']."', '".$data['lastname']."', '".$data['street']."', '".$data['street_nr']."', '".$data['zipcode']."', '".$data['city']."', '".$data['country']."', '". $data['email'] ."', '".$data['firstname_s']."', '".$data['lastname_s']."', '".$data['street_s']."', '".$data['street_nr_s']."', '".$data['zipcode_s']."', '".$data['city_s']."', '".$data['country_s']."' , '". $_SERVER['REMOTE_ADDR'] ."')";
    
    //puts collected data into database
    $db->makeQuery($query);

    //get information to send with mollie request
    $query = "SELECT order_nr, email From new_orders_external WHERE serial='".$serial."'";
    $result_id = $db->getQuery($query);
    $order_id = $result_id[0]['order_nr'];
    $email = $result_id[0]['email'];

    //making new mollie object and inserting API key
    $mollie = new Mollie_API_Client;
    $mollie->setApiKey($settings['apikey']);
    
    //trying to create payment
    try
    {
        $payment = $mollie->payments->create( 
            array(
                'amount'      => $data['total'],
                'description' => 'Aankoop bij dutch piccolo',
                'redirectUrl' => $settings['returnURL']."".$order_id,
                'webhookUrl'  => $settings['webhookURL'],
                'metadata'    => array(
                    'order_id' => $order_id,
                    'serial'   => $serial,
                    'email'    => $email
                )
            )
        );
        //Send the customer off to complete the payment.
        $url = array();
        $url[0] = $payment->getPaymentUrl();
        echo json_encode($url);
        exit;
    }
        //when payment fails give error
        catch (Mollie_API_Exception $e)
    {
        echo "API call failed: " . htmlspecialchars($e->getMessage()) . " on field " + htmlspecialchars($e->getField());
//        $to = 'thomas.heijmans@newartsint.com';
//        $from = "auto-error-massage@dutchpiccolo.nl";
//        $subject = "Your order at dutch piccolo";
//        $body = "someone tried to request a payment page but failed".date_format(date_create(), 'Y-m-d'). " " .$data['firstname']. " ". $data['lastname']. " " .$data['email'];
//        $SMTPMail = new smtpMailer($to,$from,$subject,$body);
    }
?>