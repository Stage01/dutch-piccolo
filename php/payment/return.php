<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>webshop</title>

    <link rel="stylesheet" href="../../css/main.css"> 
    <link rel="stylesheet" href="../../css/bootstrap.min.css"> 
    <script  src="../../js/bootstrap.min.js"></script>
    <style>
        button{
            color:black;
            border:none;
            padding:5px;
        }   
    </style>
    
  </head>
 
  <body>
 
    <div class="wrapper">
        <!--navigation-->
        <div class="container">
            <div class="col-xs-12 home">
                <div class="huge-outer">
                    <div class="col-xs-12 head-col huge">

                            <div class="row header"></div>
                                <div class="textbox">
                                    <h2>Thank you for your order</h2>
                                    <p>You will receive an email conformation shortly after your payment has been received.</p>

                                    <a href="../../index.html"><button>go back to home</button></a>
                                </div>
                            <div class="push"></div>

                    </div>
                </div>  
            </div>
        </div> 
      
    <footer>
        <div class="container">
            <p class="col-xs-12"></p>
        </div>
    </footer>
    
  </body>
</html>